package eu.umg.bioinformatics;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.text.DecimalFormat;

import java.util.*;

@Path("/VUS/v1")
public class PipelineRessource {

    private Map<String, String> gene_replacements;

    private Map<String, Double> ppv;

    private Map<String, Double> npv;

    /**
     * requests UniProtFetcher and provides information as input to all prediction
     * tools to form an overall score
     * 
     * @param genesymbol: HUGO gene symbol
     * @param variant:    protein description of variant
     * @param genom_pos:  genomic position of protein description
     * @throws java.net.MalformedURLException
     * @throws java.io.UnsupportedEncodingException
     * @return: Map containing prediction results of all requested services
     * @throws IOException
     */
    @GET
    @Path("/toPipeline")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> getAllPredictionResults(
            @QueryParam("genesymbol") @Parameter(in = ParameterIn.QUERY, name = "genesymbol", example = "TP53", description = "The according gene symbol") String genesymbol,
            @QueryParam("variant") @Parameter(in = ParameterIn.QUERY, name = "variant", description = "Variant (Protein-level)", example = "R282W") String variant,
            @QueryParam("genompos") @Parameter(in = ParameterIn.QUERY, name = "genompos", description = "Variant (Genomic location)", example = "chr17:g.7673776G>A") String genom_pos)
            throws IOException, MalformedURLException, UnsupportedEncodingException {

        // initialize Map to store all prediction results
        Map<String, String> prediction_results = new HashMap<>();

        // replace gene names to get better results for UniProt query
        genesymbol = replaceGene(genesymbol);
        // System.out.println(genesymbol);

        // initialize Map to store UniProt information
        Map<String, Map<String, Map<String, String>>> uniprot_info = new HashMap<>();

        try {
            // fetch information from UniProt to requested gene
            String path_uniprot = "http://mtbvm:9020/toUniProt/ProtInfoByGeneSymbol?genesymbol=" + genesymbol;
            uniprot_info = requestUniProtService(path_uniprot);
        } catch (IOException io) {
            prediction_results.put("Pipeline", "UniProtID not found");
            return prediction_results;
        }

        // extract UniProtID and Ensembl IDs to request other services
        String uniprot_id = uniprot_info.get(genesymbol).get("gene_info").get("UniProtID");

        // request FATHMM-DB with received gene information
        String path_fathmm = "http://mtbvm:9004/FATHMM/v1/toFATHMM?uniprotname=" + uniprot_id + "&variant="
                + variant + "&weights=Cancer";
        Map<String, Map<String, String>> fathmm_info = requestPredictionService(path_fathmm);

        // request Missense3D-DB with received gene information
        String path_m3d = "http://mtbvm:9008/M3D/v1/toDB?uniprot=" + uniprot_id + "&variant=" + variant;
        Map<String, Map<String, String>> m3d_info = requestPredictionService(path_m3d);

        // request VEPFetcher
        String path_vep = "http://mtbvm:9006/SIFT/v1/toVEP?genompos=" + genom_pos;
        Map<String, Map<String, String>> vep_info_raw = requestPredictionService(path_vep);
        Map<String, Map<String, String>> vep_info = new HashMap<>();
        vep_info.put(variant, vep_info_raw.get(genom_pos));

        // calculate score
        Double score = calculateScore(fathmm_info, m3d_info, vep_info, variant);
        String prediction = "";

        // write prediction string dependent from score
        if (score < 0) {
            prediction = "deleterious";
        } else if (score == 0) {
            prediction = "no information provided";
        } else {
            prediction = "neutral";
        }

        // check if a score is available for all prediction tools before storing into
        // prediction_results
        if (fathmm_info.get(variant).size() > 1) {
            prediction_results.put("FATHMM", fathmm_info.get(variant).get("Prediction"));
        } else {
            prediction_results.put("FATHMM", "-");
        }

        if (m3d_info.get(variant).size() > 1) {
            prediction_results.put("Missense3D", m3d_info.get(variant).get("Prediction"));
        } else {
            prediction_results.put("Missense3D", "-");
        }

        if (vep_info.get(variant).size() > 1) {
            prediction_results.put("SIFT", vep_info.get(variant).get("Prediction"));
        } else {
            prediction_results.put("SIFT", "-");
        }

        prediction_results.put("Pipeline", prediction);
        prediction_results.put("Score", score.toString());

        return prediction_results;

    }

    @GET
    @Metered
    @Path("/info")
    @Operation(summary = "Get information about the tool.")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponse(responseCode = "200", description = "Version data etc.")
    @APIResponse(responseCode = "400", description = "Call to the tool failed.")
    /**
     * Shows some auxiliary data for the tool
     * 
     * @return A response object
     */
    public Response getToolData() {

        String[] curl_command = new String[] { "curl", "-X", "GET", "--header", "PRIVATE-TOKEN: YqQhg-yv_3vG7t9ubDf3",
                "-L", "http://gitlab.gwdg.de/api/v4/projects/12061/repository/branches/master" };
        ProcessBuilder pb = new ProcessBuilder(curl_command);
        pb.redirectErrorStream(true);
        try {
            final Process p = pb.start();
            String[] fields = new String[] { "short_id", "title", "authored_date" };
            String json_string = "";
            String final_string = "{\"title\": \"VUSPredict\", \"data\": {\"FATHMMdb\": \"v2.3\", \"ensembl_vep\": \"latest\"}, ";
            BufferedReader output_reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = output_reader.readLine()) != null) {
                json_string = json_string.concat(line);
            }
            String cc_string = Files.readString(Paths.get("/commit_data/current_commit.json")).replace("\n", "");
            String[] cc_array = cc_string.split(",");
            for (String part : cc_array) {
                if (part.contains("short_id")) {
                    final_string = final_string.concat(part.replace("short_id", "current_commit_id") + ",");
                    break;
                }
            }
            String[] json_array = json_string.split(",");
            for (String part : json_array) {
                for (String field : fields) {
                    if (part.contains(field)) {
                        switch (field) {
                            case "short_id":
                                part = part.replace("short_id", "new_commit_id");
                                break;
                            case "title":
                                part = part.replace("title", "new_commit_message");
                                break;
                            case "authored_date":
                                part = part.replace("authored_date", "new_commit_date");
                                part = part.replaceAll("T.*$", "\""); // Replaces everything after the T
                                break;
                        }
                        final_string = final_string.concat(part + ",");
                    }
                }
            }
            final_string = final_string.substring(0, final_string.length() - 1); // Removes final comma
            final_string = final_string.concat("}");
            System.out.println(final_string);
            output_reader.close();
            return Response.ok(final_string).build();
        } catch (Exception e) {
            System.out.println("Exception while querying metadata from GitLab");
            e.printStackTrace();
        }
        return Response.status(Status.BAD_REQUEST).build();
    }

    /**
     * requests UniProt service to extract gene-specific information
     * 
     * @param path: request URL
     * @return: Map with gene-specific information
     * @throws MalformedURLException
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private Map<String, Map<String, Map<String, String>>> requestUniProtService(String path)
            throws MalformedURLException, UnsupportedEncodingException, IOException {

        // initialize new Map
        Map<String, Map<String, Map<String, String>>> uniprot_info = new HashMap<>();

        // build URL from given path_uniprot
        URL url = new URL(path);

        URLConnection connection = url.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) connection;

        httpConnection.setRequestProperty("Accept", "application/json");

        InputStream response = connection.getInputStream();
        int responseCode = httpConnection.getResponseCode();

        if (responseCode != 200) {
            throw new IllegalArgumentException();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
        StringBuilder builder = new StringBuilder();

        String output;
        while ((output = reader.readLine()) != null)
            builder.append(output);

        // System.out.println(output);
        uniprot_info = new ObjectMapper().readValue(builder.toString(), Map.class);

        return uniprot_info;

    }

    /**
     * requests prediction service via http
     * 
     * @param path: request URL
     * @return: result Map of requested predictio service
     * @throws MalformedURLException
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private Map<String, Map<String, String>> requestPredictionService(String path)
            throws MalformedURLException, UnsupportedEncodingException, IOException {

        // initialise new Map
        Map<String, Map<String, String>> prediction_info = new HashMap<>();

        // build URL from given path_uniprot
        URL url = new URL(path);

        URLConnection connection = url.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) connection;

        httpConnection.setRequestProperty("Accept", "application/json");

        InputStream response = connection.getInputStream();
        int responseCode = httpConnection.getResponseCode();

        if (responseCode != 200) {
            throw new IllegalArgumentException();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
        StringBuilder builder = new StringBuilder();

        String output;
        while ((output = reader.readLine()) != null)
            builder.append(output);

        // System.out.println(output);
        prediction_info = new ObjectMapper().readValue(builder.toString(), Map.class);
        return prediction_info;

    }

    /**
     * replaces genes to enhance performance of information extraction (if
     * necessary)
     * 
     * @param gene: gene symbol being extracted from service input
     * @return replaced gene
     * @throws FileNotFoundException
     * @throws IOException
     */
    private String replaceGene(String gene) throws FileNotFoundException, IOException {

        if (gene_replacements == null) {

            // initialize HashMap with genes that need to be replaced
            gene_replacements = new HashMap<>();

            // object of fileReader class with csv file as parameter
            System.out.println("Trying to open geneReplacements");
            FileReader fileReader = new FileReader("/mnt/geneReplacements.csv");
            // create BufferedReader object passing file reader as input
            BufferedReader csvReader = new BufferedReader(fileReader);
            // traverse row-wise through csv file and store genes and its replacements in
            // Map
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] row_data = row.split(",");
                String old_gene = row_data[0];
                String new_gene = row_data[1];
                gene_replacements.put(old_gene, new_gene);
            }
            csvReader.close();

        }

        // P53 does not result in recommended outcome, therefore it has to be changed
        // to TP53
        if (gene_replacements.containsKey(gene)) {
            gene = (String) gene_replacements.get(gene);
        }

        return gene;

    }

    /**
     * calculates prediction score based on ppv and npv
     * 
     * @param fathmm_info: prediction result of FATHMM
     * @param m3d_info:    prediction result of Missense3D
     * @param vep_info:    prediction result of VEP including SIFT prediction
     * @param variant:     actual variant to be requested
     * @return: score between -1.00 (deleterious) and 1.00 (neutral)
     */
    private double calculateScore(Map<String, Map<String, String>> fathmm_info,
            Map<String, Map<String, String>> m3d_info, Map<String, Map<String, String>> vep_info, String variant)
            throws FileNotFoundException, IOException {

        // set format of score
        DecimalFormat df = new DecimalFormat("##.##");

        // initialize score
        double score = 0.00;

        // calculate score based on positive predictive value (PPV) and negative
        // predictive value (NPV)
        // based on outcome of internship prediction tool evaluation of P53
        // check if ppv is already filled with ppv values
        if (ppv == null) {

            // initialize HashMap with genes that need to be replaced
            ppv = new HashMap<>();

            // object of fileReader class with csv file as parameter
            System.out.println("Trying to open ppv");
            FileReader fileReader = new FileReader("/mnt/ppv.csv");
            // create BufferedReader object passing file reader as input
            BufferedReader csvReader = new BufferedReader(fileReader);
            // traverse row-wise through csv file and store genes and its replacements in
            // Map
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] row_data = row.split(",");
                String tool = row_data[0];
                Double ppv_value = Double.valueOf(row_data[1]);
                ppv.put(tool, ppv_value);
            }
            csvReader.close();

        }

        // check if npv is already filled with npv values
        if (npv == null) {

            // initialize HashMap with genes that need to be replaced
            npv = new HashMap<>();

            // object of fileReader class with csv file as parameter
            FileReader fileReader = new FileReader("/mnt/npv.csv");
            // create BufferedReader object passing file reader as input
            BufferedReader csvReader = new BufferedReader(fileReader);
            // traverse row-wise through csv file and store genes and its replacements in
            // Map
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] row_data = row.split(",");
                String tool = row_data[0];
                Double npv_value = Double.valueOf(row_data[1]);
                npv.put(tool, npv_value);
            }
            csvReader.close();

        }

        // get predictions from all prediction tools
        // if warning is != "", a prediction was not possible
        String warning_fathmm = fathmm_info.get(variant).get("Warning");
        String pred_fathmm = "";
        if (warning_fathmm.equals("")) {
            pred_fathmm = fathmm_info.get(variant).get("Prediction");
        }
        String warning_m3d = m3d_info.get(variant).get("Warning");
        String pred_m3d = "";
        if (warning_m3d.equals("")) {
            pred_m3d = m3d_info.get(variant).get("Prediction");
        }
        String warning_sift = vep_info.get(variant).get("Warning");
        String pred_sift = "";
        if (warning_sift.equals("")) {
            pred_sift = vep_info.get(variant).get("Prediction");
        }

        // initialize counting variable for number of tools requested
        double number_of_requested_tools = 0.00;

        // check if tools predicted variant to be deleterious
        // only assess prediction if there is no warning message - otherwise there is no
        // prediction available
        if (warning_fathmm.equals("")) {
            if (pred_fathmm.contains("CANCER")) {
                score += ppv.get("FATHMM");
            } else {
                score += npv.get("FATHMM");
            }
            number_of_requested_tools += 1.00;
        }

        if (warning_m3d.equals("")) {
            if (pred_m3d.contains("Damaging")) {
                score += ppv.get("Missense3D");
            } else {
                score += npv.get("Missense3D");
            }
            number_of_requested_tools += 1.00;
        }

        if (warning_sift.equals("")) {
            if (pred_sift.contains("deleterious")) {
                score += ppv.get("SIFT");
            } else {
                score += npv.get("SIFT");
            }
            number_of_requested_tools += 1.00;
        }

        // devide score by number of requested prediction tools
        if (number_of_requested_tools > 0.00) {
            score /= number_of_requested_tools;
        }

        // format score to two decimals
        score = Double.valueOf(df.format(score));

        // return calculated score
        return score;

    }

}
