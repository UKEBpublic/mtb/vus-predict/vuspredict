# VUSPredict

## Getting started

Before starting this application, check for existence of FATHMM-app, FATHMM-db, Missense3D-DB, UniProtFetcher and VEPFetcher. Furthermore, check for
individual starting options, provided in each README.md file.

To start VUSPredict first run (in VUSPredict directory): $ mvn package -Pnative -Dnative-image.docker-build=true

## Functionality

###  Input

HUGO gene name, protein description of alteration and genomic description of alteration, specified by
/toPipeline?genesymbol=TP53&variant=R282W&genompos=chr17:g.7673776G>A

## Output

HashMap containing all prediction results.

Example for request /toPipeline?genesymbol=TP53&variant=R282W&genompos=chr17:g.7673776G>A:

{FATHMM:	CANCER,
 Missense3D:	Neutral,
 SIFT:		deleterious,
 Pipeline:	deleterious
}

## Running VUSPredict as docker container

To start VUSPredict, run (in VUSPredict directory): $ docker-compose up --build

Without the --build flag, code changes are not considered.

## Updating pipeline VUSPredict

To implement a new service into the VUSPredict pipeline, several steps have to be followed.

### Build new service

Before implementation, the service has to be build using a RESTful web service.

Thereby, the input should be one of the input formats used to start VUSPredict and the output should look like this (for aforementioned example):

{R282W:	{ "Prediction":	[in here goes your service prediction],
	  "Score":	[in here goes the service score],
	  "Warning":	"" (empty if no warning occurred, otherwise in here goes your warning message)
	}
}

Expose your service to a specific port on localhost that is still free (5000s for Python Flask and 8080s for Java Quarkus)

### Implement service into VUSPredict (PipelineRessource.java script)

After fetching gene information from UniProt, the request of all services starts.

To implement your service into the pipeline, add the request of your service to the other requests (recognizable at comments request ...).

Therefore, specify an URL to request your service, starting with localhost, your tool name and afterwards the request path.

Example for Missense3D: String path_m3d = "http://m3dapp:5001/toM3D_DB?uniprot=" + uniprot_id + "&variant=" + variant;

Afterwards, the result of the new service has to be implemented into prediction_results HashMap.

Therefore, add another if-else loop to the code block commented with "check if a score is available for all prediction tools before storing into
prediction_results".

Example for Missense3D:

	if (m3d_info.get(variant).size() > 1) {
		prediction_results.put("Missense3D", m3d_info.get(variant).get("Prediction"));
	}
	else {
		prediction_results.put("Missense3D", "-");
	} 

Now, the prediction result of your service should be returned.

### Update docker-compose.yml

Next, add an entry for the new service in the docker-compose.yml to link the container.

This is necessary because VUSPredict cannot assess other container without linkage.

Write the additional entry for the service in the services section.

Thereby, the first name is the name of your service.

Furthermore, you can specify a container name and expose the output to a specific port.

It is important to choose the same exposed port as specified in the request path in PipelineRessource.java.

After adding the entry, add a new dependency for your service within the vuspredict section with the condition "service_started".

This makes sure that your service starts before vuspredict is started.

Additionally, add a link to your service withon the links section of VUSPredict to make your service visible to VUSPredict.

If your service contains volumes, read the corresponding docker section on how to build volumes.

### Evaluate PPV and NPV of your service

To evaluate PPV and NPV of your service, simply run the R script reevaluate_ppv_and_npv.r within the VUSPredict_pipeline_analysis directory.

Just make sure that you added an additional column in the result data frame, within the HTTP request and an additional row for the statistical values.

The script consists of two parts which need to be commented out while executing the other one.

Further information can be found in the comments of the script.

Run each part of the script separately before moving to the next step.

### Updating PPV and NPV

After running the R script, the values for PPV and NPV can be found within the VUSPredict_pipeline_analysis directory within data/ppv.csv and
data/npv.csv.

Manually add the values of these parameters to src/main/resources/ppv.csv and src/main/resources/npv.csv within the VUSPredict directory.

Values within ppv.csv receive a "-" in front of their value as -1 shows a damaging prediction and +1 a neutral.

### Updating the score calculation

The function calculateScore within the PipelineRessource.java script calculates the score based on NPV and PPV of all tools.

Add another prediction to the other predictions to the code block under the comment "get prediction results from all prediction tools".

Example for Missense3D:

	String warning_m3d = m3d_info.get(variant).get("Warning");
        String pred_m3d = "";
        if (warning_m3d.equals("")) {
            pred_m3d = m3d_info.get(variant).get("Prediction");
        }

Also, add another entry for your service under the code block with the comment "check if tools predicted variants to be deleterious".

Example for Missense3D:

	if (warning_m3d.equals("")) {
            if (pred_m3d.contains("Damaging")) {
                score += ppv.get("Missense3D");
            }
            else {
                score += npv.get("Missense3D");
            }
            number_of_requested_tools += 1.00;
        }

Do not forget to use the right term for damaging variants that your service returns.

### Re-build VUSPredict

As the Code has changed, first run (in your VUSPredict directory): $ mvn package -Pnative -Dnative-image.docker-build=true

Afterwards, rebuild the pipeline, running (in your VUSPredict directory): $ docker-compose up --build

Now, the pipeline should be successfully updated.

### Analyzing pipeline with new implemented service

To re-evaluate the pipeline performance, run the R scipt reevaluate_pipeline.r within the VUSPredict_pipeline_analysis directory.

For further information, read the comments in the script.
